import pytest
from unittest import mock

from vengeance.protocol.impl import arksurvivalprotocol as ARKSurvivalProtocol
from vengeance.bot.impl import tamecalcbot as TameCalcBot
#from vengeance_bot_ark_tamecalc import TameCalc


class TestRePlayerSpeaks:

	@pytest.fixture(autouse=True)
	def setup(self):
		self.re = TameCalcBot._re_player_speaks
		self.ascii = '2017.09.12_03.36.31: Cromica (Cromica): no'
		self.unicode_fudgery = '2017.09.12_03.37.43: ?????? (Shinra88z): Nate is a             '

	def test_ascii_matches(self):
		assert self.re.search(self.ascii) is not None

	def test_unicode_fudgery_matches(self):
		assert self.re.search(self.unicode_fudgery) is not None

	def test_ascii_groups(self):
		match = self.re.search(self.ascii)
		assert '2017' == match.group('year')
		assert '09' == match.group('month')
		assert '12' == match.group('day')
		assert '03' == match.group('hour')
		assert '36' == match.group('minute')
		assert '31' == match.group('second')
		assert 'Cromica' == match.group('player')
		assert 'no' == match.group('message')

	def test_unicode_fudgery_groups(self):
		match = self.re.search(self.unicode_fudgery)
		assert '2017' == match.group('year')
		assert '09' == match.group('month')
		assert '12' == match.group('day')
		assert '03' == match.group('hour')
		assert '37' == match.group('minute')
		assert '43' == match.group('second')
		assert 'Shinra88z' == match.group('player')
		assert 'Nate is a' == match.group('message')

class Settings:
	pass


class TameCalcTestSetup:

	def setup(self, event_loop):
		self.loop = event_loop
		self.bot = TameCalc
		self.bot.settings = Settings
