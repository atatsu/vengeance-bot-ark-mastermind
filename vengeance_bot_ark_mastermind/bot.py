import re

from vengeance.bot import Bot, BotSetting
from vengeance.protocol.impl import arksurvivalprotocol as ARKSurvivalProtocol

class ARKMastermindBot(Bot):
	"""
	A utility bot that other bots can utilize. Assists other bots in implementing a
	command system. Dependent bots are able to register player-issued commands they wish
	to be made aware of.  Registered commands are accompanied by a handler method that
	should be invoked whenever said command is issued.
	"""

	protocols = [ARKSurvivalProtocol]

	command_prefix = BotSetting(
		default='!',
		description='Symbol that denotes a player-issued command',
	)

	# 2017.09.12_02.17.50: Atatsu (Atatsu): shit
	# 2017.09.12_03.36.31: Cromica (Cromica): no
	# 2017.09.12_03.37.43: ?????? (Shinra88z): Nate is a [oh, Ben]

	# looks to be platform handle (steam, psn, whatever) followed by in-game name
	# we only care about the in-game name since ARK doesn't handle
	# unicode characters at all ('??????'!) and we need a definite
	# way to indentify people
	_re_player_speaks = re.compile(r'''
		^(?P<year>[0-9]{4})
		\.(?P<month>[0-9]{2})
		\.(?P<day>[0-9]{2})
		_(?P<hour>[0-9]{2})
		\.(?P<minute>[0-9]{2})
		\.(?P<second>[0-9]{2})
		:\s.+?\s\((?P<player>.+?)\)
		:\s(?P<message>.*?)\s*?$
	''', re.X)

	@ARKSurvivalProtocol.subscribe_with_filter
	def re_player_speaks(self, data):
		return TameCalcBot._re_player_speaks.search(data)

	@re_player_speaks.handler
	async def check_command(self, match):
		self.log.debug('player spoke! {} - {}'.format(match.group('player'), match.group('message')))

	async def on_start(self):
		self.log.debug("I started")
