# Vengeance ARK Mastermind bot

*Mastermind* is a convenience bot whose sole purpose is to
orchestrate other bots who rely on player input for their
decision making. Command bots are a prime example of the intended
audience. 

### Dependencies

#### Protocols
 * vengeance-protocol-ark
